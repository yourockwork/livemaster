$( document ).ready(function() {

    function new_record(arr) {
        var html='';
        $.each(arr.newrecord, function(index, record){
            html+=  '<div class="js-tbl__line tbl__line">' +
                        '<div class="tbl__cell">' +
                            '<div class="d-inline-block hidden-lg hidden-md">' +
                                '<b>id_</b>' +
                            '</div>' +
                            record.id +
                        '</div>'+
                        '<div class="tbl__cell">'+
                            '<div class="b-label d-inline-block hidden-lg hidden-md mb-5px">'+
                                '<b>Проекты:</b>'+
                            '</div>'+
                            '<form class="js-form-sel-projects">'+
                                '<input type="hidden" name="idrecord" value="'+ record.id +'">' +
                                '<select class="js-select-projects form-control">';
                                    $.each(arr.projects, function(index2, project){
                                        var selected = (Number(record.projectid)==Number(project.id))?'selected':"";
                                        html+='<option value="'+project.id+'" '+selected+'>'+ project.name +'</option>'
                                    });
                                html+='</select>' +
                            '</form>' +
                        '</div>' +
                        '<div class="tbl__cell">'+
                            '<div class="b-label d-inline-block hidden-lg hidden-md mb-5px">'+
                                '<b>Сотрудники:</b>'+
                            '</div>'+
                            '<form class="js-form-sel-workers">'+
                                '<input type="hidden" name="idrecord" value="'+ record.id +'">' +
                                '<select class="js-select-workers form-control">';
                                    $.each(arr.workers, function(index2, worker){
                                        var selected = (Number(record.workerid)==Number(worker.id))?'selected':"";
                                        html+='<option value="'+worker.id+'" '+selected+'>'+ worker.name +'</option>'
                                    });
                                html+='</select>' +
                            '</form>' +
                        '</div>' +
                        '<div class="tbl__cell">'+
                            '<div class="b-label d-inline-block hidden-lg hidden-md mb-5px">'+
                                '<b>Роли:</b>'+
                            '</div>'+
                            '<form class="js-form-sel-roles">'+
                                '<input type="hidden" name="idrecord" value="'+ record.id +'">' +
                                '<select class="js-select-roles form-control">';
                                    $.each(arr.roles, function(index2, role){
                                        var selected = (Number(record.roleid)==Number(role.id))?'selected':"";
                                        html+='<option value="'+role.id+'" '+selected+'>'+ role.name +'</option>'
                                    });
                                html+='</select>' +
                            '</form>' +
                        '</div>' +
                        '<div class="tbl__cell">'+
                            '<div class="b-label d-inline-block hidden-lg hidden-md mb-5px">'+
                                '<b>Дата начала:</b>'+
                            '</div>'+
                            '<form class="js-form-dtbegin" method="post">'+
                                '<input type="hidden" name="idrecord" value="'+ record.id +'">'+
                                '<input type="date" class="js-dt-begin form-control" value="'+ record.dtbegin +'">'+
                            '</form>' +
                        '</div>' +
                        '<div class="tbl__cell">'+
                            '<div class="b-label d-inline-block hidden-lg hidden-md mb-5px">'+
                                '<b>Дата окончания:</b>'+
                            '</div>'+
                            '<form class="js-form-dtend" method="post">'+
                                '<input type="hidden" name="idrecord" value="'+ record.id +'">'+
                                '<input type="date" class="js-dt-end form-control" value="'+ record.dtend +'">'+
                            '</form>' +
                        '</div>' +
                        '<div class="tbl__cell">' +
                            '<form class="js-form-remove" method="post">' +
                                '<input type="hidden" name="remove" value="'+record.id+'">' +
                                '<button class="js-btn-remove btn-remove">' +
                                    '<span class="glyphicon glyphicon-remove">' +
                                    '</span>' +
                                '</button>' +
                            '</form>' +
                        '</div>' +
                    '</div>'
        });
        return html;
    }

    function tbl__line(val) {
        var html =
            '<div class="js-tbl__line tbl__line">'+
                '<div class="tbl__cell ">' +
                    '<div class="d-inline-block hidden-lg hidden-md">' +
                        '<b>' +
                        'id_' +
                        '</b>' +
                    '</div>' +
                    val.id +
                '</div>' +
                '<div class="tbl__cell">' +
                    '<form class="js-form-update" method="post">' +
                        '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                        '<b>' +
                        'name: ' +
                        '</b>' +
                        '</div>' +
                        '<input type="hidden" name="updateid" value="'+val.id+'">' +
                        '<input class="js-form-edit form-edit" name="updatetxt" value="'+val.name+'" placeholder="введите текст">' +
                    '</form>' +
                '</div>' +
                '<div class="tbl__cell">' +
                    '<form class="js-form-remove" method="post">' +
                        '<input type="hidden" name="remove" value="'+val.id+'">' +
                        '<button class="js-btn-remove btn-remove">' +
                            '<span class="glyphicon glyphicon-remove">' +
                            '</span>' +
                        '</button>' +
                    '</form>' +
                '</div>' +
            '</div>';
        return html;
    };

    function tbl__body(val) {
        var html =
            '<div class="js-tbl__line tbl__line">'+
                '<div class="tbl__cell">' +
                    '<div class="d-inline-block hidden-lg hidden-md">' +
                        '<b>' +
                            'id_' +
                        '</b>' +
                    '</div>' +
                    val.id +
                '</div>' +
                '<div class="tbl__cell">' +
                    '<form class="js-form-update">' +
                            '<div class="d-inline-block hidden-lg hidden-md mb-5px">' +
                                '<b>' +
                                'name: ' +
                                '</b>' +
                            '</div>' +
                            '<input type="hidden" name="updateid" value="'+val.id+'">' +
                            '<input class="js-form-edit form-edit" name="updatetxt" value="'+val.name+'" placeholder="введите текст">' +
                    '</form>' +
                '</div>' +
                '<div class="tbl__cell">' +
                    '<form class="js-form-remove" method="post">' +
                        '<input type="hidden" name="remove" value="'+val.id+'">' +
                        '<button class="js-btn-remove btn-remove">' +
                            '<span class="glyphicon glyphicon-remove">' +
                            '</span>' +
                        '</button>' +
                    '</form>' +
                '</div>' +
            '</div>'
        return html;
    };

    /* update records */
    $('.js-records').on('submit','.js-form-add', function () {
        var $this=$(this);
        var $text=$this.find('.js-txt-add');
        if(!$text.val() || $text.val() == ''){
            $text.attr('placeholder','Вы не ввели текст');
            $text.addClass('js-form-edit--err');
        }
        else {
            $text.removeClass('js-form-edit--err');
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'add',
                    text : $text.val()
                }
            })
            .done(function(data) {
                var arr=JSON.parse(data);
                $.each(arr, function(index, val){
                    $this.parents('.js-records').find('.js-tbl_body').append(
                        tbl__line(val)
                    );
                    $text.val('');
                    $text.attr('placeholder','Введите текст');
                });
            });
        }
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-remove', function () {
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'remove',
                remove : $(this).find('input[name="remove"]').val()
            }
        })
        .done(function() {
            $this.parents('.js-tbl__line').remove();
        });
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-update', function () {
        var $this=$(this);
        var $updateid=$this.find('input[name="updateid"]');
        var $updatetxt=$this.find('input[name="updatetxt"]');
        $updatetxt.removeClass('js-form-edit--change');
        if(!$updatetxt.val() || $updatetxt.val() == '' || !$updateid.val() || $updateid.val() == '') {
            $updatetxt.attr('placeholder','Введите текст');
            $updatetxt.removeClass('js-form-edit--success');
            $updatetxt.addClass('js-form-edit--err');
        }
        else {
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'update',
                    updateid : $updateid.val(),
                    updatetxt : $updatetxt.val(),
                }
            })
            .done(function() {
                $updatetxt.addClass('js-form-edit--success');
            });
        }
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('focus','.js-form-update', function () {
        var $this=$(this);
        var $updatetxt=$this.find('input[name="updatetxt"]');
        $updatetxt.removeClass('js-form-edit--err');
        $updatetxt.attr('placeholder','введите текст');
        $updatetxt.addClass('js-form-edit--change');
    });

    $('.js-records').on('blur','.js-form-update', function () {
        var $this=$(this);
        var $updateid = $this.find('input[name="updateid"]');
        var $updatetxt = $this.find('input[name="updatetxt"]');
        $updatetxt .removeClass('js-form-edit--change');
        if(!$updatetxt.val() || $updatetxt.val() == '' || !$updateid.val() || $updateid.val() == ''){
            $updatetxt.attr('placeholder','Введите текст');
            $updatetxt.addClass('js-form-edit--err');
        }
        else{
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'update',
                    updateid : $updateid.val(),
                    updatetxt : $updatetxt.val(),
                }
            })
            .done(function() {
                $updatetxt .addClass('js-form-edit--success');
            });
        }
        return false; // отменяем перезагрузку страницы
    });

    /* end update records */

    // select change

    $('.js-records').on('change','.js-select-projects', function(){
        var $this=$(this);
        $this.parents('.js-form-sel-projects').submit();
    });

    $('.js-records').on('change','.js-select-workers', function(){
        var $this=$(this);
        $this.parents('.js-form-sel-workers').submit();
    });

    $('.js-records').on('change','.js-select-roles', function(){
        var $this=$(this);
        $this.parents('.js-form-sel-roles').submit();
    });

    $('.js-records').on('change','.js-dt-begin', function(){
        var $this=$(this);
        $this.parents('.js-form-dtbegin').submit();
    });

    $('.js-records').on('change','.js-dt-end', function(){
        var $this=$(this);
        $this.parents('.js-form-dtend').submit();
    });

    $('.js-records').on('submit','.js-form-sel-projects', function () {
        var $this=$(this);
        var $selectval = $this.children('.js-select-projects').val();
        var $idrecord = $this.children('input[name="idrecord"]').val();
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'select_projects',
                idrecord : $idrecord,
                selectval : $selectval
            }
        })
        .done(function() {})
        .fail(function (jqXHR, textStatus) {});
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-sel-roles', function () {
        var $this=$(this);
        var $selectval = $this.children('.js-select-roles').val();
        var $idrecord = $this.children('input[name="idrecord"]').val();
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'select_roles',
                idrecord : $idrecord,
                selectval : $selectval
            }
        })
        .done(function() {})
        .fail(function (jqXHR, textStatus) {});
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-sel-workers', function () {
        var $this=$(this);
        var $selectval = $this.children('.js-select-workers').val();
        var $idrecord = $this.children('input[name="idrecord"]').val();
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'select_workers',
                idrecord : $idrecord,
                selectval : $selectval
            }
        })
            .done(function() {})
            .fail(function (jqXHR, textStatus) {});
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-dtbegin', function () {
        var $this=$(this);
        var $val = $this.children('.js-dt-begin').val();
        var $idrecord = $this.children('input[name="idrecord"]').val();
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'dtbegin',
                idrecord : $idrecord,
                dtbegin : $val
            }
        })
        .done(function() {})
        .fail(function (jqXHR, textStatus) {});
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-dtend', function () {
        var $this=$(this);
        var $val = $this.children('.js-dt-end').val();
        var $idrecord = $this.children('input[name="idrecord"]').val();
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'dtend',
                idrecord : $idrecord,
                dtend : $val
            }
        })
        .done(function() {})
        .fail(function (jqXHR, textStatus) {});
        return false; // отменяем перезагрузку страницы
    });

    $('.js-records').on('submit','.js-form-add-new', function () {
        var $this=$(this);
        var $project = $this.find('.js-select-new-project').val();
        var $worker = $this.find('.js-select-new-worker').val();
        var $role = $this.find('.js-select-new-role').val();
        var $dtbegin = $this.find('input[name="dtbegin"]').val();
        var $dtend = $this.find('input[name="dtend"]').val();
        if(!$project || $project == '' || !$worker || $worker == '' || !$role || $role == '' || !$dtbegin || $dtbegin == '' || !$dtend || $dtend == ''){
            $('.js-err').show();
        }
        else{
            $.ajax({
                datatype: 'json',
                type : 'POST',
                data: {
                    action : 'addnew',
                    project : $project,
                    worker : $worker,
                    role : $role,
                    dtbegin : $dtbegin,
                    dtend : $dtend
                }
            })
            .done(function(data) {
                    var arr=JSON.parse(data);
                    $this.parents('.js-records').find('.js-tbl_body').append(
                        new_record(arr)
                    );
                })
            .fail(function (jqXHR, textStatus) {});
        }
        return false; // отменяем перезагрузку страницы
    });

    // end select change

    $('.js-records').on('click','.js-sort-up-numb', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_id'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-numb', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_id'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-workers', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_workers'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-workers', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_workers'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-roles', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_roles'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-roles', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_roles'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-projects', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_projects'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-projects', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_projects'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $.each(arr, function(index, val){
                $this.parents('.js-records').find('.js-tbl_body').append(
                    tbl__body(val)
                );
            });
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-id-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_id'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $this.parents('.js-records').find('.js-tbl_body').append(
                new_record(arr)
            );

        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-id-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_id'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $this.parents('.js-records').find('.js-tbl_body').append(
                new_record(arr)
            );
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-projects-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_projects'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-projects-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_projects'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-roles-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_roles'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-roles-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_roles'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-workers-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_workers'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-workers-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_workers'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-date-begin-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_dtbegin'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $this.parents('.js-records').find('.js-tbl_body').append(
                new_record(arr)
            );
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-date-begin-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_dtbegin'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $this.parents('.js-records').find('.js-tbl_body').append(
                new_record(arr)
            );
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-up-date-end-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortup_dtend'
            }
        })
        .done(function(data) {
            var arr=JSON.parse(data);
            $this.parents('.js-records').find('.js-tbl__line').remove();
            $this.parents('.js-records').find('.js-tbl_body').append(
                new_record(arr)
            );
        });
        return false;
    });

    $('.js-records').on('click','.js-sort-down-date-end-pw', function(){
        var $this=$(this);
        $.ajax({
            datatype: 'json',
            type : 'POST',
            data: {
                action : 'sortdown_dtend'
            }
        })
            .done(function(data) {
                var arr=JSON.parse(data);
                $this.parents('.js-records').find('.js-tbl__line').remove();
                $this.parents('.js-records').find('.js-tbl_body').append(
                    new_record(arr)
                );
            });
        return false;
    });

    /* end sort */

});