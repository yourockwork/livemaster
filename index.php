<?php
//ini_set('display_errors', 1);
require_once 'config.php';  // подключаем файл конфигурации
require_once 'app/includes/db.php'; // класс работы с базой данных
require_once 'app/includes/mongoDataBase.php'; // класс работы с базой данных
require_once 'app/includes/cache.php'; // класс работы с memcache
require_once 'app/core/model.php';  // ядро модели
require_once 'app/core/view.php';   // ядро представления
require_once 'app/core/controller.php'; // ядро контроллера
require_once 'app/core/route.php';  // класс маршрутизации
require_once 'app/includes/Twig/Autoloader.php'; // шаблонизатор twig
require_once 'app/includes/vendor/autoload.php'; // mongodb

DB::connect(); // подключаемся к mysql базе
MongoDataBase::connect();
Cache::connect(); // кэширование
Route::start(); // запускаем маршрутизатор