<?php

class Controller_workers extends Controller
{

	function __construct()
	{
		$this->model = new Model_workers();
		$this->view = new View();
	}
	
	function action_index()
	{
		if(! empty($_POST["action"]))
        {
                switch($_POST["action"])
                {
                    case 'add':
                        return $this->model->add();
                        break;

                    case 'remove':
                        return $this->model->remove();
                        break;

                    case 'sortup_id':
                        return $this->model->sortup_id();
                        break;

                    case 'sortdown_id':
                        return $this->model->sortdown_id();
                        break;

                    case 'sortup_workers':
                        return $this->model->sortup_workers();
                        break;

                    case 'sortdown_workers':
                        return $this->model->sortdown_workers();
                        break;

                    case 'update':
                        return $this->model->update();
                        break;
                }
        }
		$data = $this->model->get_data();		
		$this->view->twig('', 'template_workers.html', $data);
	}
}