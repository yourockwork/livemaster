<?php

class Controller_mongo extends Controller
{
	function __construct()
	{
		$this->model = new Model_mongo();
		$this->view = new View();
	}

	function action_index()
	{
		/* обработка ajax запросов */
		if(! empty($_POST["action"]))
		{
			switch($_POST["action"])
			{
				case 'remove':
					return $this->model->remove($_POST['remove']);
					break;
				case 'add':
					return $this->model->add($_POST['text']);
					break;
				case 'update':
					return $this->model->update($_POST['updateid'], $_POST['updatetxt']);
					break;
				case 'sortup_names':
					return $this->model->sortup_names();
					break;
				case 'sortdown_names':
					return $this->model->sortdown_names();
					break;
			}
		}

		$data = $this->model->get_data();
		$this->view->twig('', 'template_mongo.html', $data);
	}
}