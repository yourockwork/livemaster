<?php

class Controller_projects extends Controller
{

	function __construct()
	{
		$this->model = new Model_projects();
		$this->view = new View();
	}
	
	function action_index()
	{
		/* обработка ajax запросов */
		if(! empty($_POST["action"]))
        {
                switch($_POST["action"])
                {
                    case 'add':
                        return $this->model->add();
                        break;

                    case 'remove':
                        return $this->model->remove();
                        break;

                    case 'sortup_id':
                    return $this->model->sortup_id();
                    break;

                    case 'sortdown_id':
                    return $this->model->sortdown_id();
                    break;

                    case 'sortup_projects':
                    return $this->model->sortup_projects();
                    break;

                    case 'sortdown_projects':
                    return $this->model->sortdown_projects();
                    break;

                    case 'update':
                    return $this->model->update();
                    break;

                }
        }       
		$data = $this->model->get_data();
        $this->view->twig('', 'template_projects.html', $data);
	}
}