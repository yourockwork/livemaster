<?php

class Controller_pw extends Controller
{
	function __construct()
	{
		$this->model = new Model_pw();
		$this->view = new View();
	}
	
	function action_index()
	{
        /* обработка ajax запросов */
        if(! empty($_POST["action"]))
        {
            switch($_POST["action"])
            {
                case 'sortup_id':
                    return $this->model->sortup_id();
                    break;

                case 'sortdown_id':
                    return $this->model->sortdown_id();
                    break;

                case 'sortup_projects':
                    return $this->model->sortup_projects();
                    break;

                case 'sortdown_projects':
                    return $this->model->sortdown_projects();
                    break;

                case 'sortup_roles':
                    return $this->model->sortup_roles();
                    break;

                case 'sortdown_roles':
                    return $this->model->sortdown_roles();
                    break;

                case 'sortup_workers':
                    return $this->model->sortup_workers();
                    break;

                case 'sortdown_workers':
                    return $this->model->sortdown_workers();
                    break;

                case 'sortup_dtbegin':
                    return $this->model->sortup_dtbegin();
                    break;

                case 'sortdown_dtbegin':
                    return $this->model->sortdown_dtbegin();
                    break;

                case 'sortup_dtend':
                    return $this->model->sortup_dtend();
                    break;

                case 'sortdown_dtend':
                    return $this->model->sortdown_dtend();
                    break;

                case 'addnew':
                    return $this->model->addnew();
                    break;

                case 'remove':
                    return $this->model->remove();
                    break;

                case 'select_projects':
                    return $this->model->select_projects();
                    break;

                case 'select_roles':
                    return $this->model->select_roles();
                    break;

                case 'select_workers':
                    return $this->model->select_workers();
                    break;

                case 'dtbegin':
                    return $this->model->dtbegin();
                    break;

                case 'dtend':
                    return $this->model->dtend();
                    break;
            }
        }
		$data = $this->model->get_data();
        $this->view->twig('', 'template_pw.html', $data);
	}
}