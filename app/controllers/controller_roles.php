<?php

class Controller_roles extends Controller
{

	function __construct()
	{
		$this->model = new Model_roles();
		$this->view = new View();
	}

	function action_index()
	{
		/* обработка ajax запросов */
		if(! empty($_POST["action"]))
        {
                switch($_POST["action"])
                {
                    case 'add':
                        return $this->model->add();
                        break;

                    case 'sortup_id':
                    return $this->model->sortup_id();
                    break;

                    case 'sortdown_id':
                    return $this->model->sortdown_id();
                    break;

                    case 'sortup_roles':
                    return $this->model->sortup_roles();
                    break;

                    case 'sortdown_roles':
                    return $this->model->sortdown_roles();
                    break;

                    case 'remove':
                    return $this->model->remove();
                    break;

                    case 'update':
                    return $this->model->update();
                    break;

                }
        }
        
		$data = $this->model->get_data();
        $this->view->twig('', 'template_roles.html', $data);
	}
}