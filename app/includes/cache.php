<?php
/**
 * class Cache
 *
 * Кэширование при помощи MemCache
 */

class Cache
{
    private static $memcached;

    public static function connect($host='localhost',$port=11211)
    {
        self::$memcached = new Memcache();
        self::$memcached->connect($host,$port);
    }

    public static function get($name)
    {
        return self::$memcached->get($name);
    }

    public static function save($name, $data)
    {
        self::$memcached->set($name, $data);
    }

    public static function delete($name)
    {
        if ($name)
        {
            self::$memcached->delete($name);
        }
        else
        {
            self::$memcached->flush();
        }
    }
}