<?php
/**
 * class DB
 * данный класс необходим для работы с базой данных
 */

class DB {
    protected static $connection;

    public static function connect($host=DB_HOST, $user=DB_LOGIN, $password=DB_PASSWORD, $db_name=DB_NAME){
        self::$connection = new mysqli($host, $user, $password, $db_name);
        self::query("SET NAMES UTF8");
        if( !self::$connection) {
            throw new Exception('Could not connect to DB ');
        }
    }

    public static function last_id(){
        if ( !self::$connection ){
            return false;
        }
        $result = self::$connection->insert_id;
        if ( self::$connection->error){
            throw new Exception(self::$connection->error);
        }
        if ( is_int($result) ){
            return $result;
        }
        else{
            return false;
        }
    }

    public static function query($sql){
        if ( !self::$connection){
            return false;
        }
        $result = self::$connection->query($sql);
        if ( self::$connection->error){
            throw new Exception(self::$connection->error);
        }
        if ( is_bool($result) ){
            return $result;
        }
        $data = array();
        while($row = $result->fetch_assoc()){
            $data[] = $row;
        }
        $result->free_result(); // результаты запроса нам больше не нужны, чистим память
        return $data;
    }

    public static function escape($str){
        return self::$connection->escape_string($str);
    }
}