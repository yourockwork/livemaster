<?php

class MongoDataBase{
	protected static $connection;
	protected static $db;

	public static function connect(){
		self::$connection = new MongoDB\Client();
		self::$db = self::$connection->selectDatabase(DB_NAME);
	}

	public static function closeConnect(){

	}

	public static function getCollection($collection){
		return self::$db->selectCollection($collection);
	}

	public static function getCollectionArr($collection){
		return self::getCollection($collection)->find()->toArray();
	}

	public static function getCollectionArrbyId($collection, $id){
		return self::getCollection($collection)->find([
			'_id' => new MongoDB\BSON\ObjectId($id)
		])->toArray();
	}

	// из-за `_id` в названии колонки коллекции mongo, и из-за особенностей mongodb->toarray(),
	// пришлось написать этот `костыльный` метод
	public static function getCollectionByIdForJSON($collection, $id){
		$res = [];

		$arr = self::getCollection($collection)->find([
			'_id' => new MongoDB\BSON\ObjectId($id)
		]);

		foreach ($arr as $key => $entry){
			$res[$key]["id"] = (string) $entry['_id'];
			$res[$key]["name"] = $entry['name'];
		}

		return $res;
	}

	/*
	public static function getSortCollectionForJSON($collection, $id){

	}
	*/

	public static function getCollectionbyId($collection, $id){
		return self::getCollection($collection)->find([
			'_id' => new MongoDB\BSON\ObjectId($id)
		]);
	}

	/**
	 * @param $collection
	 * @param $text
	 * @return \MongoDB\BSON\ObjectId
	 * вернет `id` добавленной записи
	 */
	public static function addCollectionDoc($collection, $text){
		$id = new MongoDB\BSON\ObjectId();
		self::getCollection($collection)->insertOne([
			'_id' => $id,
			'name' => $text
		]);

		return $id;
	}

	public static function updateCollectionDoc($collection, $id, $text){
		$id = new MongoDB\BSON\ObjectId($id);
		self::getCollection($collection)->updateOne(
			[
				'_id' => $id
			],
			[
				'$set' =>
					[
						'name' => $text
					]
			]
		);
	}

	public static function delCollectionDoc($collection, $id){
		return self::getCollection($collection)->deleteOne([ '_id' => new MongoDB\BSON\ObjectId ($id) ]);
	}

}