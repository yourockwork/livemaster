<?php

class View
{
    function twig($content_view, $template_view, $data = null)
    {
        Twig_Autoloader::register();
        $loader = new Twig_Loader_Filesystem('app/templates');
        $twig = new Twig_Environment($loader, array(
            'auto_reload' => true
        ));
        echo $twig->render($template_view, array('content_view' => $content_view,'data' => $data));
    }
}