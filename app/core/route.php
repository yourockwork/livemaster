<?php

/**
 * class Route
 * данный класс выступает в роли маршрутизатора
 * в нём мы определяем запрашиваемую страницу 
 * цепляем соответствующие классы контроллеров и моделей
 * перенаправляем на 404-ую страницу если нужный нам контроллер отсутствует
 * для контроллеров страниц создаем экземпляры
 * вызываем действия по умолчанию для этих контроллеров
 */

class Route
{
	static function start()
	{
		// задаем контроллер по умолчанию
		$controller_name = 'Main';
		// задаем действие по умолчанию
		$action_name = 'index';
		// разбиваем url
		$routes = explode('/', $_SERVER['REQUEST_URI']);
		// получаем из разобранного url имя соответствующего контроллера
		if (!empty($routes[1]))
		{	
			$controller_name = $routes[1];
		}
		// получаем имя экшн
		if (!empty($routes[2]))
		{
			$action_name = $routes[2];
		}
		// добавляем префиксы
		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;
		// подцепляем файл с классом модели
		$model_file = strtolower($model_name).'.php';
		$model_path = "app/models/".$model_file;
		if(file_exists($model_path))
		{
			include_once "app/models/".$model_file;
		}
		// подцепляем файл с классом контроллера
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = "app/controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			include_once "app/controllers/".$controller_file;
		}
		else
		{
			// редирект на 404 страницу
			Route::ErrorPage404(); 
		}
		// создаем контроллер
		$controller = new $controller_name;
		$action = $action_name;
		if(method_exists($controller, $action))
		{
			// вызываем действие контроллера,
            // если главная страница сайта, то по умолчанию будет action_index
			$controller->$action();
		}
		else
		{
			Route::ErrorPage404();
		}
	}

	function ErrorPage404()
	{
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
    }
}