<?php
/*
 * модель "Сотрудники"
 * */
class Model_workers extends Model
{
	public function get_data()
	{
        try {
            if ( !$result = Cache::get('workers') )
            {
                $result = DB::query('select * from exam_workers');
                Cache::save('workers', $result);
            }
            return $result;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
	}

    public function update()
    {
        try {
            if ( Cache::get('workers') )
            {
                Cache::delete('workers');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('update exam_workers set worker_lastname="'.DB::escape($_POST['updatetxt']).'" where worker_id="'.DB::escape($_POST['updateid']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function add(){
        try {
            if ( Cache::get('workers') )
            {
                Cache::delete('workers');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('insert into exam_workers (worker_lastname) VALUES ("'.DB::escape($_POST['text']).'")');
            $result=DB::query('select worker_id as id, worker_lastname as name from exam_workers where worker_id="'.DB::last_id().'"');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function remove(){
        try {
            if ( Cache::get('workers') )
            {
                Cache::delete('workers');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('delete from exam_workers where worker_id="'.DB::escape($_POST['remove']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

	public function sortup_id(){
        try {
            $result=DB::query('select worker_id as id, worker_lastname as name from exam_workers ORDER BY worker_id ASC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_id(){
        try {
            $result=DB::query('select worker_id as id, worker_lastname as name from exam_workers ORDER BY worker_id DESC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_workers(){
        try {
            $result=DB::query('select worker_id as id, worker_lastname as name from exam_workers ORDER BY worker_lastname ASC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_workers(){
        try {
            $result=DB::query('select worker_id as id, worker_lastname as name from exam_workers ORDER BY worker_lastname DESC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}