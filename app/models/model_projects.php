<?php
/*
 * модель "Проекты"
 * */
class Model_projects extends Model
{
	public function get_data()
	{
        try {
            if ( !$result = Cache::get('projects') )
            {
                $result = DB::query('select * from exam_projects');
                Cache::save('projects', $result);
            }
            return $result;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
	}

    public function add(){
        try {
        	// добавляем новый проект, старый удаляем из кэша
            if ( Cache::get('projects') )
            {
                Cache::delete('projects');
                // аналогично поступаем с pw
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }

			// добавить данные
			DB::query('insert into exam_projects (project_name) VALUES ("'.DB::escape($_POST['text']).'")');
			// получить последнюю добавленную запись
			$result=DB::query('select project_id as id, project_name as name from exam_projects where project_id="'.DB::last_id().'"');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function update()
    {
        try {
            if ( Cache::get('projects') )
            {
                Cache::delete('projects');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('update exam_projects set project_name="'.DB::escape($_POST['updatetxt']).'" where project_id="'.DB::escape($_POST['updateid']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function remove(){
        try {
            if ( Cache::get('projects') )
            {
                Cache::delete('projects');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('delete from exam_projects where project_id="'.DB::escape($_POST['remove']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_id(){
        try {
            $result=DB::query('select project_id as id, project_name as name from exam_projects ORDER BY project_id ASC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_id(){
        try {
            $result=DB::query('select project_id as id, project_name as name from exam_projects ORDER BY project_id DESC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_projects(){
        try {
            $result=DB::query('select project_id as id, project_name as name from exam_projects ORDER BY project_name ASC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_projects(){
        try {
            $result=DB::query('select project_id as id, project_name as name from exam_projects ORDER BY project_name DESC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}