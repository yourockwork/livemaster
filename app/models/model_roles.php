<?php

/*
 * модель "Роли"
 * */
class Model_roles extends Model
{
	public function get_data()
	{
        try {
            if ( !$result = Cache::get('roles') )
            {
                $result = DB::query('select * from exam_roles');
                Cache::save('roles', $result);
            }
            return $result;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
	}

    public function add(){
        try {
            if ( Cache::get('roles') )
            {
                Cache::delete('roles');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('insert into exam_roles (role_name) VALUES ("'.DB::escape($_POST['text']).'")');
            $result=DB::query('select role_id as id, role_name as name from exam_roles where role_id="'.DB::last_id().'"');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function update()
    {
        try {
            if ( Cache::get('roles') )
            {
                Cache::delete('roles');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }

            DB::query('update exam_roles set role_name="'.DB::escape($_POST['updatetxt']).'" where role_id="'.DB::escape($_POST['updateid']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function remove(){
        try {
            if ( Cache::get('roles') )
            {
                Cache::delete('roles');
                if ( Cache::get('pw') )
                {
                    Cache::delete('pw');
                }
            }
            DB::query('delete from exam_roles where role_id="'.DB::escape($_POST['remove']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_id(){
        try {
            $result=DB::query('select role_id as id, role_name as name from exam_roles ORDER BY role_id ASC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_id(){
        try {
            $result=DB::query('select role_id as id, role_name as name from exam_roles ORDER BY role_id DESC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_roles(){
        try {
            $result=DB::query('select role_id as id, role_name as name from exam_roles ORDER BY role_name ASC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_roles(){
        try {
            $result=DB::query('select role_id as id, role_name as name from exam_roles ORDER BY role_name DESC');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}