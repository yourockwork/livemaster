<?php
/*
 * модель "Список"
 * */
class Model_pw extends Model
{
	public function get_data()
    {
        try {
            if ( !$result = Cache::get('pw') )
            {
                $result = [];
                $result['projects_workers'] = DB::query('select * from 
                                                          exam_projects_workers as epw, 
                                                          exam_roles as er, 
                                                          exam_projects as ep, 
                                                          exam_workers as ew 
                                                          WHERE 
                                                          ep.project_id=epw.project_id AND 
                                                          ew.worker_id=epw.worker_id AND 
                                                          er.role_id=epw.role_id 
                                                          order by ep_id ASC');
                $result['projects'] = DB::query('select * from exam_projects');
                $result['workers'] = DB::query('select * from exam_workers');
                $result['roles'] = DB::query('select * from exam_roles');
                Cache::save('pw', $result);
            }
            return $result;
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function remove(){
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            DB::query('delete from exam_projects_workers where ep_id="'.DB::escape($_POST['remove']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function select_projects(){
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            DB::query('update exam_projects_workers set project_id="'.DB::escape($_POST['selectval']).'" where ep_id="'.DB::escape($_POST['idrecord']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function select_workers(){
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            DB::query('update exam_projects_workers set worker_id="'.DB::escape($_POST['selectval']).'" where ep_id="'.DB::escape($_POST['idrecord']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function select_roles(){
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            DB::query('update exam_projects_workers set role_id="'.DB::escape($_POST['selectval']).'" where ep_id="'.DB::escape($_POST['idrecord']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function dtbegin()
    {
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            DB::query('update exam_projects_workers set dt_begin="'.DB::escape($_POST['dtbegin']).'" where ep_id="'.DB::escape($_POST['idrecord']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function dtend()
    {
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            DB::query('update exam_projects_workers set dt_end="'.DB::escape($_POST['dtend']).'" where ep_id="'.DB::escape($_POST['idrecord']).'"');
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    public function addnew()
    {
        try {
            if ( Cache::get('pw') )
            {
                Cache::delete('pw');
            }
            $result = [];
            DB::query(
            'insert into exam_projects_workers 
                (
                project_id,
                worker_id,
                role_id,
                dt_begin,
                dt_end
                ) 
                VALUES 
                (
                "'.DB::escape($_POST['project']).'", 
                "'.DB::escape($_POST['worker']).'",
                "'.DB::escape($_POST['role']).'",
                "'.DB::escape($_POST['dtbegin']).'",
                "'.DB::escape($_POST['dtend']).'")'
            );
            $result['newrecord'] = DB::query(
                'select ep_id as id, 
                project_id as projectid, 
                worker_id as workerid, 
                role_id as roleid, 
                dt_begin as dtbegin, 
                dt_end as dtend 
                from exam_projects_workers
                where ep_id="'.DB::last_id().'"'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_id(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid, 
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend 
                    from 
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    epw.ep_id ASC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_id(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid, 
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend 
                    from 
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    WHERE 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by epw.ep_id DESC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_projects(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid, 
                    ep.project_name as projectname,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    ep.project_name ASC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_projects(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid, 
                    ep.project_name as projectname,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    ep.project_name DESC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_workers(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid,
                    ew.worker_lastname as workername,  
                    epw.role_id as roleid,
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    ew.worker_lastname ASC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_workers(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid,
                    ew.worker_lastname as workername,  
                    epw.role_id as roleid,
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    ew.worker_lastname DESC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_roles(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid,
                    er.role_name as rolename, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    er.role_name ASC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_roles(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid,
                    er.role_name as rolename, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    er.role_name DESC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_dtbegin(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    epw.dt_begin ASC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_dtbegin(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    epw.dt_begin DESC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortup_dtend(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    epw.dt_end ASC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sortdown_dtend(){
        $result = [];
        try {
            $result['newrecord'] = DB::query(
                'select  
                    epw.ep_id as id, 
                    epw.project_id as projectid,
                    epw.worker_id as workerid, 
                    epw.role_id as roleid, 
                    epw.dt_begin as dtbegin, 
                    epw.dt_end as dtend
                    from
                    exam_projects_workers as epw, 
                    exam_roles as er, 
                    exam_projects as ep, 
                    exam_workers as ew 
                    where 
                    ep.project_id=epw.project_id AND 
                    ew.worker_id=epw.worker_id AND 
                    er.role_id=epw.role_id 
                    order by 
                    epw.dt_end DESC'
            );
            $result['projects'] = DB::query('select project_id as id, project_name as name from exam_projects');
            $result['workers'] = DB::query('select worker_id as id, worker_lastname as name from exam_workers');
            $result['roles'] = DB::query('select role_id as id, role_name as name from exam_roles');
            echo json_encode($result);
            exit();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }

}