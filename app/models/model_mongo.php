<?php
/*
 * Модель "mongo"
 * данные в MongoDB
 * */

class Model_mongo extends Model
{
	public function get_data()
	{
		/*
		$result = MongoDataBase::getCollectionArr('mongo');
		echo "<pre>";
			print_r($result);
		echo "<pre>";
		foreach ($result as $item){
			echo $item->name;
		}*/


		try {
			if ( !$result = Cache::get('mongo') )
			{
				$result = MongoDataBase::getCollectionArr('mongo');
				//$result = $this->getmongo();

				Cache::save('mongo', $result);
			}

			return $result;
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}

	}

	public function add($text){
		try {
			if ( Cache::get('mongo') )
			{
				Cache::delete('mongo');
			}
			// добавить данные
			$id = MongoDataBase::addCollectionDoc('mongo', $text)->__toString();
			// последняя добавленная запись
			$result = MongoDataBase::getCollectionByIdForJSON('mongo', $id);
			echo json_encode($result);
			exit();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function remove($id){
		//return "re";
		try {
			if ( Cache::get('mongo') )
			{
				Cache::delete('mongo');
			}

			MongoDataBase::delCollectionDoc('mongo', $id);
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function update($id, $text)
	{
		try {
			if ( Cache::get('mongo') )
			{
				Cache::delete('mongo');
			}
			MongoDataBase::updateCollectionDoc('mongo', $id, $text);
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function sortup_names(){
		try {
			$result=[];
			echo json_encode($result);
			exit();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function sortdown_names(){
		try {
			$result=[];
			echo json_encode($result);
			exit();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	/*
	public function getmongo(){
		return MongoDataBase::getCollectionArr('mongo');
		//return self::$db->mongo->find()->toArray();
	}
	*/
}