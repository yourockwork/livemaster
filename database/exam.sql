-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 29 2019 г., 11:37
-- Версия сервера: 5.7.23
-- Версия PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `exam`
--

-- --------------------------------------------------------

--
-- Структура таблицы `exam_projects`
--

CREATE TABLE `exam_projects` (
  `project_id` int(11) NOT NULL COMMENT 'ID проекта',
  `project_name` varchar(255) DEFAULT NULL COMMENT 'название проекта'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Проекты компании';

--
-- Дамп данных таблицы `exam_projects`
--

INSERT INTO `exam_projects` (`project_id`, `project_name`) VALUES
(1, 'Project A'),
(2, 'Project B'),
(3, 'Project C'),
(4, 'Project D'),
(5, 'Project E');

-- --------------------------------------------------------

--
-- Структура таблицы `exam_projects_workers`
--

CREATE TABLE `exam_projects_workers` (
  `ep_id` int(11) NOT NULL COMMENT 'primary ID',
  `project_id` int(11) DEFAULT NULL COMMENT 'ID из exam_projects ',
  `worker_id` int(11) DEFAULT NULL COMMENT 'ID из exam_workers',
  `role_id` int(11) DEFAULT NULL COMMENT 'ID из exam_roles',
  `dt_begin` date DEFAULT NULL COMMENT 'дата начала работы сотрудника над проектом',
  `dt_end` date DEFAULT NULL COMMENT 'дата окончания работы сотрудника над проектом'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Занятость сотрудников в проектах компании';

--
-- Дамп данных таблицы `exam_projects_workers`
--

INSERT INTO `exam_projects_workers` (`ep_id`, `project_id`, `worker_id`, `role_id`, `dt_begin`, `dt_end`) VALUES
(14, 1, 3, 3, '2019-03-28', '2019-04-01'),
(15, 2, 4, 5, '1998-01-21', '2014-09-01'),
(16, 2, 6, 4, '2000-05-05', '2017-01-07'),
(20, 1, 1, 6, '2018-07-05', '2018-07-05'),
(22, 5, 13, 1, '2018-10-25', '2018-10-27'),
(23, 1, 1, 1, '2018-02-15', '2018-02-15');

-- --------------------------------------------------------

--
-- Структура таблицы `exam_roles`
--

CREATE TABLE `exam_roles` (
  `role_id` int(11) NOT NULL COMMENT 'ID роли',
  `role_name` varchar(255) DEFAULT NULL COMMENT 'название роли (например, DEV, PM, QA и т.п.)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Проектные роли';

--
-- Дамп данных таблицы `exam_roles`
--

INSERT INTO `exam_roles` (`role_id`, `role_name`) VALUES
(1, 'Developer'),
(2, 'PR Manager'),
(3, 'Designer'),
(4, 'Art Director'),
(5, 'Tech Director'),
(6, 'YouRock'),
(7, 'fghfgh'),
(8, 'hjklhjkljhkl'),
(9, 'jjkjk');

-- --------------------------------------------------------

--
-- Структура таблицы `exam_workers`
--

CREATE TABLE `exam_workers` (
  `worker_id` int(11) NOT NULL COMMENT 'ID сотрудника',
  `worker_lastname` varchar(255) DEFAULT NULL COMMENT 'фамилия сотрудника'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сотрудники компании';

--
-- Дамп данных таблицы `exam_workers`
--

INSERT INTO `exam_workers` (`worker_id`, `worker_lastname`) VALUES
(1, 'M. Zukerberg'),
(2, 'B. Geits'),
(3, 'S. Jobs'),
(4, 'S. Brin'),
(5, 'I. Mask'),
(6, 'A. Lebedev'),
(8, 'T. Cook'),
(9, 'J. Ive'),
(13, 'S. Wozniakk'),
(15, 'youroc');

-- --------------------------------------------------------

--
-- Структура таблицы `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `new_columntex` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `test`
--

INSERT INTO `test` (`id`, `number`, `new_columntex`) VALUES
(1, 1, 'B'),
(2, 1, 'A'),
(3, 3, 'C'),
(4, 3, 'D'),
(5, 4, 'E'),
(6, 4, 'F');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `exam_projects`
--
ALTER TABLE `exam_projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Индексы таблицы `exam_projects_workers`
--
ALTER TABLE `exam_projects_workers`
  ADD PRIMARY KEY (`ep_id`);

--
-- Индексы таблицы `exam_roles`
--
ALTER TABLE `exam_roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Индексы таблицы `exam_workers`
--
ALTER TABLE `exam_workers`
  ADD PRIMARY KEY (`worker_id`);

--
-- Индексы таблицы `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `exam_projects`
--
ALTER TABLE `exam_projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID проекта', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `exam_projects_workers`
--
ALTER TABLE `exam_projects_workers`
  MODIFY `ep_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary ID', AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `exam_roles`
--
ALTER TABLE `exam_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID роли', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `exam_workers`
--
ALTER TABLE `exam_workers`
  MODIFY `worker_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID сотрудника', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
